import requests
import datetime as dt

class ArrivalPrediction:

    def __init__(self, stop_id):

        self.base_url = "http://realtime.mbta.com/developer/api/v2/predictionsbystop"
        self.params = {"api_key": "wX9NwuHnZU2ToO7GmGR9uw",
                       "format": "json",
                       "stop": stop_id}
        self.r = requests.get(self.base_url, params=self.params)

    def json_response(self):
        """
        Method to return Arrival Prediction in JSON format
        :return: JSON object of Arrival Prediction response
        """
        return self.r.json()

    def next_arrivals(self):
        """
        Method to parse next arrivals from Arrival Prediction response
        :return: Sorted list of arrival times
        """
        json = self.json_response()
        trips = json['mode'][0]['route'][0]['direction'][0]['trip']
        next_trips = [trips[i]['pre_dt'] for i in range(len(trips))]
        next_trips.sort()
        return [dt.datetime.fromtimestamp(int(next_trip_time)) for next_trip_time in next_trips]


    def time_until_next(self):
        """
        Method to return difference between next arrivals and now on Arrival Predictions
        :return: List of time deltas between next arrivals and now
        """
        now = dt.datetime.now()
        return [trip_time - now for trip_time in self.next_arrivals()]


#create new Arrival Prediction object with northbound red line Park Street stop ID
northbound_red = ArrivalPrediction(70076)
for x in northbound_red.time_until_next():
    print x